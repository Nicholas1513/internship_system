from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    session, \
    redirect




# PURPOSE: Displays the main index page
@app.route('/', methods = ['GET'])
def index():
  return render_template("views/index/indexView.html", config         =         config,
                                                       navBarConfig   =   navBarConfig
                                                       )

