from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    session



# PURPOSE: Employer Landing Page
@app.route('/employerLanding', methods = ['GET'])
@require_role('employer')
def employerLanding():
    role = session['role']
    positions = Internship.select()
    return render_template("views/employers/employerLandingView.html",  config         =         config,
                                                                        navBarConfig   =   navBarConfig,
                                                                        positions      =      positions,
                                                                        role           =           role)


