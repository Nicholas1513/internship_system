from wtforms import Form, BooleanField, StringField, PasswordField, validators
from application.models.internshipModel import Internship 
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    redirect, \
    session



class employerNewPost(Form):
    supervisor_name                 = StringField('Supervisor Name',                        [validators.Length(min=4, max=25)])
    supervisor_phone                = StringField('Supervisor Phone',                       [validators.Length(min=6, max=35)])
    supervisor_email                = StringField('Supervisor Email',                       [validators.Length(min=10, max=35)])
    title_of_position               = StringField('Title of Internship Position',           [validators.Length(min=4, max=25)])
    description_of_position         = StringField('Description of Internship Position',     [validators.Length(min=6, max=35)])
    location_of_internship          = StringField('Location of Internship (City & State)',  [validators.Length(min=4, max=35)])
    requirements                    = StringField('Requirements for the Internship',        [validators.Length(min=4, max=25)])
    class_levels_considered         = StringField('Class levels considered',                [validators.Length(min=4, max=25)])
    majors_considered               = StringField('Majors Considered',                      [validators.Length(min=6, max=35)])
    deadline                        = StringField('Deadline',                               [validators.Length(min=4, max=15)])
    duration_of_internship          = StringField('Duration of Internship',                 [validators.Length(min=6, max=35)])
    start_date                      = StringField('Start Date',                             [validators.Length(min=4, max=15)])
    hourly_rate                     = StringField('Hourly Rate',                            [validators.Length(min=6, max=35)])
    compensation                    = StringField('Compensation',                           [validators.Length(min=4, max=35)])
    other_stipend                   = StringField('Other Stipend / Perks',                  [validators.Length(min=4, max=25)])
   
    
# PURPOSE: Employer posting new internship!
@app.route('/employerNewPost/<action>', methods = ['POST'])
@require_role('employer')
def employerNewPosting(action):
  role = session['role']
  user = session['user']
  form = employerNewPost(request.form)
  print action
  while (request.method == 'POST' and form.validate()):
    if action == "save":
      
      newPost = Internship.create(supervisor_name             =               form.supervisor_name.data,
                                  supervisor_phone            =              form.supervisor_phone.data,
                                  supervisor_email            =              form.supervisor_email.data,
                                  title_of_position           =             form.title_of_position.data,
                                  description_of_position     =       form.description_of_position.data,
                                  location_of_internship      =        form.location_of_internship.data,
                                  requirements                =                  form.requirements.data,
                                  class_levels_considered     =       form.class_levels_considered.data,
                                  majors_considered           =             form.majors_considered.data,
                                  deadline                    =                      form.deadline.data,
                                  duration_of_internship      =        form.duration_of_internship.data,
                                  start_date                  =                    form.start_date.data,
                                  hourly_rate                 =                   form.hourly_rate.data,
                                  compensation                =                  form.compensation.data,
                                  other_stipend               =                 form.other_stipend.data, 
                                  posted_by                   =                                    user,
                                  is_submitted                =                                   False,
                                  is_approved                 =                                   0)
      newPost.save()
      print "Saved"
      return redirect(url_for('employerLanding'))
    
    elif action == "submit":
      newPost = Internship.create(supervisor_name             =               form.supervisor_name.data,
                                  supervisor_phone            =              form.supervisor_phone.data,
                                  supervisor_email            =              form.supervisor_email.data,
                                  title_of_position           =             form.title_of_position.data,
                                  description_of_position     =       form.description_of_position.data,
                                  location_of_internship      =        form.location_of_internship.data,
                                  requirements                =                  form.requirements.data,
                                  class_levels_considered     =       form.class_levels_considered.data,
                                  majors_considered           =             form.majors_considered.data,
                                  deadline                    =                      form.deadline.data,
                                  duration_of_internship      =        form.duration_of_internship.data,
                                  start_date                  =                    form.start_date.data,
                                  hourly_rate                 =                   form.hourly_rate.data,
                                  compensation                =                  form.compensation.data,
                                  other_stipend               =                 form.other_stipend.data, 
                                  posted_by                   =                                    user,
                                  is_submitted                =                                    True,
                                  is_approved                 =                                       0)
      newPost.save()
      print "Submitted"
      return redirect(url_for('employerLanding'))
    
  print "Aborting"
  return abort(403)


@app.route('/employerNewPost', methods = ['GET'])
@require_role('employer')
def employerNewPostGet():  
  print "Starting new post"
  role = session['role']
  form = employerNewPost(request.form)
  return render_template("views/employers/employerNewPostView.html", config           =         config, 
                                                                     navBarConfig     =   navBarConfig,
                                                                     form             =           form,
                                                                     role             =           role)