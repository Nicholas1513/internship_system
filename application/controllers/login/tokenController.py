from application.logic.validation import require_role
from application.models.tokenModel import Token
from application.models.userModel import User
from application.models import *
from application.config import *
from application import app
import datetime
import base64
import uuid
import re
from flask import \
    render_template, \
    request, \
    redirect, \
    url_for


def createToken(mail):
  """
  :Purpose: Creates a token and groups it with a given email
  :Input: the email you would like to create a token for
  :Return: a token string
  """
  token = base64.b64encode(uuid.uuid4().hex).decode('utf-8')
  # Control the token experation time by changing the value of timedelta in the next line
  Token.create(string     =                                                 token,
               email      =                                                  mail,
               timeStamp  = datetime.datetime.now()+datetime.timedelta(minutes=5))
  return token


# PURPOSE: This creates a token string
@app.route('/token/<email>', methods = ['GET'])
def token(email):
  print email
  urlStr = createToken(email)
  return redirect(url_for("timeStampCheck", urlStr = urlStr, email = email))    

