from application import app
from application.models import *
from application.config import *
from application.logic.validation import require_role
from flask import \
    render_template, \
    request, \
    url_for




# PURPOSE: Test url link.
@app.route('/url/<urlStr>/', methods = ['GET', 'POST'])
def url(urlStr):
  trialUrl = "flask-login-test-nicholas1513.c9users.io:8080/sessionTesting/"+urlStr
  print trialUrl
  return render_template("views/login/urlView.html",   config  = config,
                                                      trialUrl = trialUrl)

