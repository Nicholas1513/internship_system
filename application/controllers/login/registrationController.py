from wtforms import Form, BooleanField, StringField, TextAreaField, validators
from application.models.organizationModel import Organization
from application.models.userModel import User
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    flash, \
    session, \
    redirect


class employerProfileCreation(Form):
    organization_name          = StringField('Name of Organization', [validators.Length(min=4, max=25)])
    web_address                = StringField('Website', [validators.Length(min=6, max=35)])
    physical_address           = StringField('Physical Address', [validators.Length(min=10, max=35)])
    organization_description   = TextAreaField('Brief Description', [validators.Length(min=4, max=25)])
    organization_type          = StringField('Type', [validators.Length(min=4, max=35)])
    username                      = StringField('Username', [validators.Length(min=4, max=35)])
    

class studentProfileCreation(Form):
    first_name = StringField('Student First Name', [validators.Length(min=4, max=25)])
    last_name = StringField('Student Last Name', [validators.Length(min=4, max=25)])
    class_level = StringField('Student Class Level', [validators.Length(min=4, max=25)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    international_status = BooleanField('I am an international student')

  


# PURPOSE: Handles registration for both employers and students.
@app.route('/register/<provided_email>', methods = ['GET','POST'])
def registration(provided_email):
  employerForm = employerProfileCreation(request.form)
  if request.method == 'POST' and employerForm.validate():
    user = User(          role                          =                   "employer",
                          active                        =                         True,
                          username                      =   employerForm.username.data,
                          email                         =               provided_email)
    user.save()
    org = Organization(   organization_name             =          employerForm.organization_name.data,
                          web_address                   =                employerForm.web_address.data,
                          organization_description      =   employerForm.organization_description.data,
                          physical_address              =           employerForm.physical_address.data,
                          organization_type             =          employerForm.organization_type.data)  
    org.save()
    user.oid = org
    user.save()
    print ("User" + user.username)
    print ("Org: " + org.organization_name)
    flash('You have created an %s account. Use %s to access this account at login.' %(user.role.role, user.email))
    return redirect(url_for('login'))
    
    
  studentForm = studentProfileCreation(request.form)
  if request.method == 'POST' and studentForm.validate():
    employerUsernameCount = 0
    stu = User(       username              =                studentForm.username.data,
                      first_name            =              studentForm.first_name.data,
                      last_name             =               studentForm.last_name.data,
                      email                 =   "Employer"+str((employerUsernameCount+1)),
                      class_level           =             studentForm.class_level.data,
                      international_status  =    studentForm.international_status.data,
                      role                  =                                'student',
                      active                =                                    False,
                      title                 =                                 'Tilted')
    stu.save()    
    flash('You have created a %s account. Use %s to access this account at login.' %(stu.role.role, stu.email))
    return redirect(url_for('login'))
  
  return render_template("views/login/registrationView.html",  config         =       config,
                                                               employerForm   = employerForm,
                                                               studentForm    =  studentForm)
                                                               
                                                               
                                                               
                                                               
