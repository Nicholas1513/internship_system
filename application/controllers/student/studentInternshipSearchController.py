from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    session



# PURPOSE: Students will be provided a list of posted internships to look through
@app.route('/studentInternshipSearch', methods = ['GET'])
@require_role('student')
def studentInternshipSearch():
  role = session['role']
  posting = Internship.select()
  return render_template("views/student/studentInternshipSearchView.html",  config        =         config,
                                                                            navBarConfig  =   navBarConfig,
                                                                            posting       =        posting,
                                                                            role          =           role)

