from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    session




# PURPOSE: Student landing page
@app.route('/studentLanding', methods = ['GET'])
@require_role('student')
def studentLanding():
   role = session['role']
   posting = Internship.select()
   return render_template("views/student/studentLandingView.html",  config          =         config,
                                                                    navBarConfig    =   navBarConfig,
                                                                    posting         =        posting,
                                                                    role            =           role)

