from wtforms import Form, BooleanField, StringField, TextAreaField, PasswordField, validators
from application.models.internshipModel import Internship
from application.logic.validation import require_role
from wtfpeewee.orm import model_form
from application.models import *
from application.config import *
from application import app
from peewee import *
from flask import \
    render_template, \
    request, \
    url_for, \
    session



# PURPOSE: Student view a single internship position
@app.route('/studentViewInternship/<iid>', methods = ['GET'])
@require_role('student')
def ViewInternshipPosition(iid):
    role = session['role']
    entry = Internship.get(Internship.iid==iid) # Entry should be getting a single internship
                                                # for which for which to extract the id and then it can be edited.
    EntryForm = model_form(Internship)
    if request.method == 'GET':
        form = EntryForm(request.form, obj=entry)
        if form.validate():
            form.populate_obj(entry)
            entry.save()
    else:
        form = EntryForm(obj=entry)
    return render_template("views/student/studentViewInternshipPositionView.html",  onfig           =       config,
                                                                                    navBarConfig    = navBarConfig,
                                                                                    form            =         form,
                                                                                    entry           =        entry,
                                                                                    role            =         role)

