from application.models.util import *

class Majorsminors(Model):
    mmid        = PrimaryKeyField()
    name        = TextField()
    prefix      = TextField()
    
    class Meta():
        database = getDB("trial")