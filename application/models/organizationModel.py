from flask_admin.contrib.peewee import ModelView
from application.models.util import *
        
class Organization(Model):
    oid                         = PrimaryKeyField()
    organization_name           = TextField()
    web_address                 = TextField()
    physical_address            = TextField()
    organization_description    = TextField()
    organization_type           = TextField()
    # email                       = TextField()     REMOVED: this field exists in the user table 
    # username                    = TextField()     REMOVED: this field exists in the user table

    
    class Meta():
        database = getDB("trial")