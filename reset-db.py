# WARNING
# This script deletes the database file and configures empty
# tables for the models defined in the models module.
from application.models.organizationModel import Organization
from application.models.majorsminorsModel import Majorsminors
from application.models.internshipModel import Internship
from application.models.roleModel import Role
from application.models.userModel import User
from application.models.util import *
from application.models import classes
from application.config import *

    
def init_db ():
  """
  :Purpose: Initializes the database
  """
  # First, we create the databases.
  for database in config.databases:
    filename = config.databases[database].filename
    # Remove the DB
    if os.path.isfile(filename):
      os.remove(filename)
    # Create an empty DB file
    open(filename, 'a').close()
  # Now, go through the modules we've discovered in the models directory.
  # Create tables for each model.
  for c in classes:
    c.create_table(True)
  print 'Initialized the database.'
  



  def refill_internship():
    """
    :Purpose: Refills the internship table in the 'trial' database
    """
    refill = raw_input("Would you like to repopulate the DB internship table?: ").lower()
    if refill == "yes" or refill == "y":
      Internship.create(supervisor_name             =                                                 "Scott H.",
                        supervisor_phone            =                                               "6666666666",
                        supervisor_email            =                               "bigCody123@bereaBubble.edu",
                        title_of_position           =                                                "Minion #9",
                        description_of_position     =                    "Must bow to supervisor every morning.",
                        location_of_internship      =                                      "Danforth Technology",
                        requirements                =                          "required to do what is required",
                        class_levels_considered     =                                                      "all",
                        majors_considered           =                           "Computer & Information Science",
                        deadline                    =                                                "Yesterday",
                        duration_of_internship      =                                                     "Life",
                        start_date                  =                                                 "Tomorrow",
                        hourly_rate                 =                                                      "$50",
                        compensation                = "$500 for counseling after taking blame for broken things",
                        other_stipend               =                                          "$1,000 just cuz",
                        posted_by                   =                                                  "heggens",
                        is_submitted                =                                                      False,
                        is_approved                 =                                                          0)
    elif refill == "no" or refill == "n":
      return
    else:
      print "You didn't select a valid response."
      refill_internship()




  def refill_organization():
    """
    :Purpose: Refills the organization table in the 'trial' database
    """
    refill = raw_input("Would you like to repopulate the DB organization table?: ").lower()
    if refill == "yes" or refill == "y":
      # Organization.create(organization_name             =                         "Berea College",
      #                     web_address                   =                         "www.berea.edu",
      #                     organization_description      =           "4 Year Liberal Arts College",
      #                     physical_address              =  "101 Chestnut St. / Berea, KY / 40404",
      #                     organization_type             =                            "Non-Profit")
      # link_to_organization = Organization.select().where(Organization.physical_address == "101 Chestnut St. / Berea, KY / 40404")
      # User.create(  oid_id = link_to_organization,
      #               role  = "employer",
      #               active = True,
      #               username = 'BereaCo',
      #               email = 'asdfadsf@berea.edu')
      
      user = User()
      user.role  = "employer"
      user.active = True
      user.username = 'BereaCo'
      user.email = 'BBBBBB@berea.edu'
      user.save()
      
      org = Organization()
      org.organization_name             =                         "Berea College"
      org.web_address                   =                         "www.berea.edu"
      org.organization_description      =           "4 Year Liberal Arts College"
      org.physical_address              =  "101 Chestnut St. / Berea, KY / 40404"
      org.organization_type             =                            "Non-Profit"
      org.save()
      
      
      print org
      #User_Organization = Organization.get(organization_name = "Berea College")
      user.oid =  org
      user.save()
      print ("user: " + user.username + " // Org: " + org.organization_name)
      
      #### TEST ####
      def test():
        if User.select().where(User.username == "BereaCo").exists():
          X = User.get(User.username == "BereaCo")
          print "THE USER IS : {0}".format(X.oid.organization_name)
          # print "THE ORGANIZATION NAME IS : {0}".format(user['organization_name'])
        else:
          print "THE QUERY IS FAILING FOR YOUR TEST"
      
      test()
    elif refill == "no" or refill == "n":
      return
    else:
      print "You didn't select a valid response."
      refill_organization()
  
  
  
  
  def refill_majorsminors():
    """
    :Purpose: Refills the majorsminors table in the 'trial' database
    """
    Majorsminors.create(name      = "Computer & Information Science",
                        prefix    =                            "CSC")
  
  
  
  
  def refill_user():
    """
    :Purpose: Refills the user table in the 'trial' database with two users
    """
    refill = raw_input("Would you like to repopulate the DB user table?: ").lower()
    if refill == "yes" or refill == "y":
      # Creates a user for testing purposes
      User.create(username                =           "detoren",
                  role                    =           "student",
                  email                   = "detoren@berea.edu",
                  active                  =              "True",
                  first_name              =              "Nick",
                  last_name               =            "DeTore",
                  phone                   =        "8505738401",
                  title                   =               "Mr.",
                  class_level             =            "Senior",
                  international_status    =        "US Citizen",
                  dept                    =                   1)
      # Creates a second user for testiing purposes
      User.create(username      =           "heggens",
                  role          =          "employer", 
                  email         = 'heggens@berea.edu',
                  active        =              'true',
                  first_name    =             "Scott",
                  last_name     =            "Heggen",
                  phone         =        "9876543210",
                  title         =               "Dr.")
    elif refill == "no" or refill == "n":
      return
    else:
      print "You didn't select a valid response."
      refill_user()
  
  
  
  
  def refill_role():
    """
    :Purpose: Refills the role table in the 'trial' database
    """
    Role.create(role  =       "student")
    Role.create(role  =      "employer")
    Role.create(role  = "administrator")
  
  
  
  # call all of the refill functions here to repopulate the database
  refill_role()
  refill_majorsminors()
  refill_internship()
  refill_organization()
  refill_user()

if __name__ == "__main__":
  init_db()
  